/*
 * MIT License
 *
 * Copyright (c) 2022 zycra
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.gitee.zycra.jdbc.enums;

/**
 * The SQL block compare condition enum, only used to create {@link com.gitee.zycra.jdbc.util.SQLBlock}.
 *
 * @author zycra
 * @since 1.0.0
 */
public enum SQLConditionEnum {

    EQUALS(" = ?"),
    GREATER_THAN(" > ?"),
    GREATER_OR_EQUALS_THAN(" >= ?"),
    LESS_THAN(" < ?"),
    LESS_OR_EQUALS_THAN(" <= ?"),
    NOT_EQUALS(" != ?"),
    BETWEEN_AND(" BETWEEN ? AND ?"),
    START_WITH(" LIKE CONCAT('%', ?)"),
    END_WITH(" LIKE CONCAT(?, '%')"),
    CONTAINS(" LIKE CONCAT('%', ?, '%')"),
    NOT_START_WITH(" NOT LIKE CONCAT('%', ?)"),
    NOT_END_WITH(" NOT LIKE CONCAT(?, '%')"),
    NOT_CONTAINS(" NOT LIKE CONCAT('%', ?, '%')"),
    IN(" IN"),
    NOT_IN(" NOT IN"),
    IS_NULL(" IS NULL"),
    IS_NOT_NULL(" IS NOT NULL");

    /**
     * The SQL block compare condition expression.
     *
     * @since 1.0.0
     */
    private final String condition;

    public String getCondition() {
        return this.condition;
    }

    SQLConditionEnum(String condition) {
        this.condition = condition;
    }
}
