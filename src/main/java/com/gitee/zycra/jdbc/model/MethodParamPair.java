/*
 * MIT License
 *
 * Copyright (c) 2022 zycra
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.gitee.zycra.jdbc.model;

/**
 * A method name and param type pair, only used to cache data object setter method param pair.
 *
 * @author zycra
 * @since 1.0.0
 */
public final class MethodParamPair {

    /**
     * Name of method.
     *
     * @since 1.0.0
     */
    private final String methodName;

    /**
     * Type of param.
     *
     * @since 1.0.0
     */
    private final Class<?> paramClass;

    public MethodParamPair(String methodName, Class<?> paramClass) {
        this.methodName = methodName;
        this.paramClass = paramClass;
    }

    public String getMethodName() {
        return methodName;
    }

    public Class<?> getParamClass() {
        return paramClass;
    }
}
