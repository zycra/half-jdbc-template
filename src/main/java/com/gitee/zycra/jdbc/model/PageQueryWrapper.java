/*
 * MIT License
 *
 * Copyright (c) 2022 zycra
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.gitee.zycra.jdbc.model;

import java.util.List;

/**
 * Page query param object, dependent SQL and param can be composed by {@link com.gitee.zycra.jdbc.util.SQLChain}.
 *
 * @author zycra
 * @since 1.0.0
 */
public final class PageQueryWrapper {

    /**
     * SQL for count total rows.
     *
     * @since 1.0.0
     */
    private final String countTotalSql;

    /**
     * SQL param for count total rows.
     *
     * @since 1.0.0
     */
    private final List<Object> countTotalSqlParam;

    /**
     * SQL for select all rows.
     *
     * @since 1.0.0
     */
    private final String querySql;

    /**
     * SQL param for select all rows.
     *
     * @since 1.0.0
     */
    private final List<Object> querySqlParam;

    /**
     * Current page number for select.
     *
     * @since 1.0.0
     */
    private final Integer page;

    /**
     * Current page size for select.
     *
     * @since 1.0.0
     */
    private final Integer size;

    public PageQueryWrapper(String countTotalSql, List<Object> countTotalSqlParam, String querySql, List<Object> querySqlParam, Integer page,
                            Integer size) {
        this.countTotalSql = countTotalSql;
        this.countTotalSqlParam = countTotalSqlParam;
        this.querySql = querySql;
        this.querySqlParam = querySqlParam;
        this.page = page;
        this.size = size;
    }

    public String getCountTotalSql() {
        return countTotalSql;
    }

    public List<Object> getCountTotalSqlParam() {
        return countTotalSqlParam;
    }

    public String getQuerySql() {
        return querySql;
    }

    public List<Object> getQuerySqlParam() {
        return querySqlParam;
    }

    public Integer getPage() {
        return page;
    }

    public Integer getSize() {
        return size;
    }
}
