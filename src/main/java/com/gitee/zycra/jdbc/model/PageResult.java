/*
 * MIT License
 *
 * Copyright (c) 2022 zycra
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.gitee.zycra.jdbc.model;

import java.util.List;

/**
 * Page query result object.
 *
 * @param <T> The type of result object.
 * @author zycra
 * @since 1.0.0
 */
public final class PageResult<T> {

    /**
     * Current page number for select.
     *
     * @since 1.0.0
     */
    private final Integer page;

    /**
     * Total rows count for select.
     *
     * @since 1.0.0
     */
    private final Integer total;

    /**
     * Current page data, size is current page size.
     *
     * @since 1.0.0
     */
    private final List<T> data;

    public PageResult(Integer page, Integer total, List<T> data) {
        this.page = page;
        this.total = total;
        this.data = data;
    }

    public Integer getPage() {
        return page;
    }

    public Integer getTotal() {
        return total;
    }

    public List<T> getData() {
        return data;
    }

    @Override
    public String toString() {
        return "PageResult{" + "page=" + page + ", total=" + total + ", data=" + data + "}";
    }
}
