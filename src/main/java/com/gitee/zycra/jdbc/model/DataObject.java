/*
 * MIT License
 *
 * Copyright (c) 2022 zycra
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.gitee.zycra.jdbc.model;

import java.util.Map;
import java.util.Set;

/**
 * Container for caches the data object resolution results.
 *
 * @author zycra
 * @since 1.0.0
 */
public final class DataObject {

    /**
     * The table name of data object, the value is from the value field of {@link com.gitee.zycra.jdbc.annotation.Table}.
     *
     * @since 1.0.0
     */
    private final String tableName;

    /**
     * The primary key column name of data object, the value is from the value of
     * {@link com.gitee.zycra.jdbc.annotation.Column} or just field name, witch field also marked with
     * {@link com.gitee.zycra.jdbc.annotation.ID}.
     *
     * @since 1.0.0
     */
    private final String idColumnName;

    /**
     * Getter method name of primary key field.
     *
     * @since 1.0.0
     */
    private final String getIdColumnMethod;

    /**
     * Java type of primary key field.
     *
     * @since 1.0.0
     */
    private final Class<?> idParamType;

    /**
     * Map for cache getter method name, the key is column name and the value is standard getter method name.
     *
     * @since 1.0.0
     */
    private final Map<String, String> getColumnMethodMap;

    /**
     * Map for cache setter method name, the key is column name and the value is standard setter method name.
     *
     * @since 1.0.0
     */
    private final Map<String, MethodParamPair> setColumnMethodMap;

    /**
     * Set of database column names used for batch insert.
     *
     * @since 1.0.0
     */
    private final Set<String> batchInsertSet;

    public DataObject(String tableName, String idColumnName, String getIdColumnMethod, Class<?> idParamType, Map<String, String> getColumnMethodMap
            , Map<String, MethodParamPair> setColumnMethodMap, Set<String> batchInsertSet) {
        this.tableName = tableName;
        this.idColumnName = idColumnName;
        this.getIdColumnMethod = getIdColumnMethod;
        this.idParamType = idParamType;
        this.getColumnMethodMap = getColumnMethodMap;
        this.setColumnMethodMap = setColumnMethodMap;
        this.batchInsertSet = batchInsertSet;
    }

    public String getTableName() {
        return tableName;
    }

    public String getIdColumnName() {
        return idColumnName;
    }

    public String getGetIdColumnMethod() {
        return getIdColumnMethod;
    }

    public Class<?> getIdParamType() {
        return idParamType;
    }

    public Map<String, String> getGetColumnMethodMap() {
        return getColumnMethodMap;
    }

    public Map<String, MethodParamPair> getSetColumnMethodMap() {
        return setColumnMethodMap;
    }

    public Set<String> getBatchInsertSet() {
        return batchInsertSet;
    }
}
