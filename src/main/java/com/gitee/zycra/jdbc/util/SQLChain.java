/*
 * MIT License
 *
 * Copyright (c) 2022 zycra
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.gitee.zycra.jdbc.util;

import com.gitee.zycra.jdbc.enums.SQLConditionEnum;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * A wrapper to chain multi {@link SQLBlock} instance.
 *
 * <p>Use {@link SQLChain#builder()} to start a new chain, use {@link SQLChain#build()} to complete the current chain.
 * <p>Use {@link SQLChain#getSQL()} to get the chain result SQL, use {@link SQLChain#getParamList()} to get the chain result params.
 *
 * @author zycra
 * @see SQLBlock
 * @since 1.0.0
 */
public final class SQLChain {

    /**
     * All SQL blocks in this chain.
     *
     * @since 1.0.0
     */
    private final List<SQLBlock> blockList = new ArrayList<>();

    /**
     * left bracket index set.
     *
     * @since 1.0.0
     */
    private final Set<Integer> leftBracketSet = new HashSet<>();

    /**
     * right bracket index set.
     *
     * @since 1.0.0
     */
    private final Set<Integer> rightBracketSet = new HashSet<>();

    /**
     * chain result SQL.
     *
     * @since 1.0.0
     */
    private final StringBuilder sql = new StringBuilder();

    /**
     * chain result params.
     *
     * @since 1.0.0
     */
    private List<Object> paramList = new ArrayList<>();

    private SQLChain() {
    }

    /**
     * Create a new chain.
     *
     * @return SQLChain instance.
     * @since 1.0.0
     */
    public static SQLChain builder() {
        return new SQLChain();
    }

    /**
     * Add a SQL block to current chain.
     *
     * @param block SQL block.
     * @return current chain.
     * @since 1.0.0
     */
    public SQLChain addBlock(SQLBlock block) {
        if (block != null) {
            this.blockList.add(block);
        }
        return this;
    }

    /**
     * Declaration of insert a left bracket to current chain position.
     *
     * @return current chain.
     * @since 1.0.0
     */
    public SQLChain addLeftBracket() {
        this.leftBracketSet.add(this.blockList.size());
        return this;
    }

    /**
     * Declaration of insert a right bracket to current chain position.
     *
     * @return current chain.
     * @since 1.0.0
     */
    public SQLChain addRightBracket() {
        this.rightBracketSet.add(this.blockList.size());
        return this;
    }

    /**
     * Resolve all SQL block in current chain.
     *
     * @return current chain.
     * @since 1.0.0
     */
    public SQLChain build() {
        if (blockList.isEmpty()) {
            return this;
        }
        boolean first = true;
        boolean lastLeftBracket = false;
        for (int i = 0, size = blockList.size(); i < size; i++) {
            SQLBlock sqlBlock = blockList.get(i);
            if (Boolean.FALSE.equals(sqlBlock.getChainCondition())) {
                if (leftBracketSet.contains(i)) {
                    sql.append(" ").append(sqlBlock.getLink()).append(" (");
                    lastLeftBracket = true;
                }
                continue;
            }
            if (first) {
                first = false;
            } else if (!lastLeftBracket) {
                sql.append(" ").append(sqlBlock.getLink());
            }
            lastLeftBracket = false;
            if (leftBracketSet.contains(i)) {
                sql.append(" (");
            }
            sql.append(" ").append(sqlBlock.getLabel());
            sql.append(getCondition(sqlBlock));
            if (rightBracketSet.contains(i + 1)) {
                sql.append(")");
            }
            paramList.addAll(sqlBlock.getParamList());
        }
        paramList = Collections.unmodifiableList(paramList);
        if (!sql.isEmpty()) {
            sql.insert(0, " WHERE");
        }
        return this;
    }

    /**
     * Get the chain result SQL.
     *
     * @return the chain result SQL.
     * @since 1.0.0
     */
    public String getSQL() {
        return this.sql.toString();
    }

    /**
     * Get the chain result params.
     *
     * @return the chain result params.
     * @since 1.0.0
     */
    public List<Object> getParamList() {
        return Collections.unmodifiableList(this.paramList);
    }

    private String getCondition(SQLBlock sqlBlock) {
        SQLConditionEnum condition = sqlBlock.getCondition();
        if (SQLConditionEnum.IN.equals(condition) || SQLConditionEnum.NOT_IN.equals(condition)) {
            return getInQuerySql(condition.getCondition(), sqlBlock.getParamList().size());
        }
        return condition.getCondition();
    }

    private String getInQuerySql(String prefix, int paramSize) {
        if (paramSize <= 0) {
            return "";
        }
        return prefix + "(" + getInParamSql(paramSize) + ")";
    }

    private String getInParamSql(int paramSize) {
        if (paramSize == 1) {
            return "?";
        }
        String halfSql = getInParamSql(paramSize >> 1);
        String allSql = halfSql + ", " + halfSql;
        return (paramSize & 1) == 1 ? allSql + ", ?" : allSql;
    }
}
