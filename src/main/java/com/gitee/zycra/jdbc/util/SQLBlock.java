/*
 * MIT License
 *
 * Copyright (c) 2022 zycra
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.gitee.zycra.jdbc.util;

import com.gitee.zycra.jdbc.enums.SQLConditionEnum;
import com.gitee.zycra.jdbc.enums.SQLLinkEnum;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * A wrapper of where condition SQL piece.
 *
 * <p>Use {@link SQLBlock#of} method to create instance.
 *
 * @author zycra
 * @see SQLLinkEnum
 * @see SQLConditionEnum
 * @since 1.0.0
 */
public final class SQLBlock {

    /**
     * SQL link type.
     *
     * @since 1.0.0
     */
    private final SQLLinkEnum link;

    /**
     * SQL label name.
     *
     * @since 1.0.0
     */
    private final String label;

    /**
     * SQL compare condition.
     *
     * @since 1.0.0
     */
    private final SQLConditionEnum condition;

    /**
     * True is chain current condition, false is do not chain.
     *
     * @since 1.0.0
     */
    private final Boolean chainCondition;

    /**
     * SQL execute param list.
     *
     * @since 1.0.0
     */
    private final List<Object> paramList;

    public static SQLBlock of(Boolean chainCondition, SQLLinkEnum link, String label, SQLConditionEnum condition, Object... paramList) {
        return new SQLBlock(chainCondition, link, label, condition, paramList);
    }

    public static SQLBlock of(SQLLinkEnum link, String label, SQLConditionEnum condition, Object... paramList) {
        return new SQLBlock(link, label, condition, paramList);
    }

    private SQLBlock(Boolean chainCondition, SQLLinkEnum link, String label, SQLConditionEnum condition, Object... paramList) {
        this.link = link;
        this.label = label;
        this.condition = condition;
        this.chainCondition = chainCondition;
        this.paramList = paramList == null ? new ArrayList<>() : Arrays.asList(paramList);
    }

    private SQLBlock(SQLLinkEnum link, String label, SQLConditionEnum condition, Object... paramList) {
        this.link = link;
        this.label = label;
        this.condition = condition;
        if (paramList == null) {
            this.chainCondition = false;
            this.paramList = new ArrayList<>();
            return;
        }
        boolean cc = true;
        List<Object> params = Arrays.asList(paramList);
        for (Object param : params) {
            if (param == null || (param instanceof String p && p.isBlank())) {
                cc = false;
                break;
            }
        }
        this.chainCondition = cc;
        this.paramList = params;
    }

    public SQLLinkEnum getLink() {
        return link;
    }

    public String getLabel() {
        return label;
    }

    public SQLConditionEnum getCondition() {
        return condition;
    }

    public Boolean getChainCondition() {
        return chainCondition;
    }

    public List<Object> getParamList() {
        return Collections.unmodifiableList(paramList);
    }
}
